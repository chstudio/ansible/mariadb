Ansible Role to manage MariaDB
------------------------------

This simple role allow you to install and configure MariaDB on a given machine.

## Example

```ansible-playbook
- name: Install MariaDB
  include_role:
    name: mariadb
  vars:
    mariadb_database: "{{ mariadb.database }}"
    mariadb_username: "{{ mariadb.username }}"
    mariadb_password: "{{ mariadb.password }}"
    mariadb_backup: true
    mariadb_backup_username: "{{ mariadb.backup.username }}"
    mariadb_backup_password: "{{ mariadb.backup.password }}"
```
